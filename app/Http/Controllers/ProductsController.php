<?php

namespace App\Http\Controllers;

use App\Products;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$cart = session()->remove('cart');
        $products = Products::all();
        return view('home', compact('products'));
    }

    public function display()
    {
        $products = Products::all();
        //$products = Products::orderBy('name', 'category')->get();
        //$products = Products::where('name', 'coffee')->get();
        //$products = Products::latest();

        return view('products',[
            'products' => $products,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function show(Products $products)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function edit(Products $products)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
   /* public function update(Request $up, Products $products)
    {
        $up=$up->get(id);

        if($up->id and $up->stock)
        {
            $cart = session()->get('cart');

            $cart[id]["stock"] = $up->stock;

            session()->put('cart', $cart);

            session()->flash('success', 'Cart updated successfully');
        }
    }*/
    public function update(Request $request, Products $products)
    {
        if($request->id and $request->stock)
        {
            $cart = session()->get('cart');

            $cart[$request->id]["stock"] = $request->stock;

            session()->put('cart', $cart);

            session()->flash('success', 'Cart updated successfully');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function destroy(Products $products)
    {
        //
    }

    public function cart()
    {
       //$cart = session()->remove('cart');
        return view('cart');
    }

    public function addToCart($id)
    {
        $products = Products::find($id);

        if(!$products) {

            abort(404);

        }

        $cart = session()->get('cart');

        // if cart is empty then this the first product
        if(!$cart) {

            $cart = [
                    $id => [
                        "id" => $products->id,
                        "name" => $products->name,
                        "stock" => 1,
                        "cost" => $products->cost,
                        "image" => $products->image
                    ]
            ];

            session()->put('cart', $cart);

            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        // if cart not empty then check if this product exist then increment stock
        if(isset($cart[$id])) {

            $cart[$id]['stock']++;

            session()->put('cart', $cart);

            return redirect()->back()->with('success', 'Product added to cart successfully!');

        }

        // if item not exist in cart then add to cart with stock = 1
       $cart[$id] = [
            "id" => $products->id,
            "name" => $products->name,
            "stock" => 1,
            "cost" => $products->cost,
            "image" => $products->image
        ];

        session()->put('cart', $cart);

        return redirect()->back()->with('success', 'Added to cart successfully!');
    }

    public function remove(Request $request)
   {
        if($request->id) {

            $cart = session()->get('cart');

            if(isset($cart[$request->id])) {

                unset($cart[$request->id]);

                session()->put('cart', $cart);
            }

            session()->flash('success', 'Product removed successfully');
        }
    }
}
