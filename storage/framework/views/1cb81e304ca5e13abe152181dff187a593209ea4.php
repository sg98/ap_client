<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title>manager</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>

  <style>
      .b{
          background-image: url("background1.jpg");
          background-repeat: none;
      }
      #index-banner{
          float: center;
      }
      .card {
        border-radius: 25px;
        float:left;
        margin: 1.5%;
        width: 30%;
      }
      .card-image{
        border-top-right-radius: 25px;
        border-top-left-radius: 25px;
      }
  </style>

</head>

<body class = "b">
  <nav class="orange n" role="navigation">
    <div class="nav-wrapper container">
      <a id="logo-container" href="#" class="brand-logo">Manager</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="login">LOGIN</a></li>
        <li><a href="signup">REGISTER</a></li>
      </ul>

      <ul id="nav-mobile" class="sidenav">
        <li><a href="welcome">HOME</a></li>
        <li><a href="login">LOGIN</a></li>
        <li><a href="signup">REGISTER</a></li>
      </ul>
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">Search</i></a>
    </div>
  </nav>


<section class="container align-center">
    <div class="container c1">
        <form class = "white" action="/submit2" method="post">
            <br>
            <?php echo csrf_field(); ?>
            <input id='dev' type='text' name="search" value="" placeholder="   enter the name of the menu item">
            <div class ="center">
                <input type="submit" name="submit" value="submit" class ="btn brand z-depth-0 orange darken-2">
            </div>
            <br>
        </form>
    </div>
</section>
 
<section class = "container align-center">
    <h4 class = "center">Results</h4>
    <br><br><br>


    <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prod): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="card c">
          <div class="card-image">
            <img class = "imgi" height="250px" width="60px" src="<?php echo e(asset($prod->image)); ?>">
          </div>
          <div class="card-content">
            <span class="card-title black-text bolder"><?php echo e($prod->name); ?></span>
            <p>Cost: <?php echo e($prod->cost); ?>

            <p>Left in Stock: <?php echo e($prod->stock); ?>

            <p>Category: <?php echo e($prod->category); ?>

            <audio controls><source src="<?php echo e($prod->audio); ?>" type="audio/mpeg">
          </div>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      
    


</section>

  

  <!--
  <footer class="page-footer orange">
    <div class="footer-copyright">
      <div class="container">
      Made by <a class="brown-text text-lighten-3" href="http://materializecss.com">Materialize</a>
      </div>
    </div>
  </footer>
-->

  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>

  </body>
</html>

<?php /**PATH C:\Users\sean9\Documents\ap\resources\views/search1.blade.php ENDPATH**/ ?>