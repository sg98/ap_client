<?php /*
    //include('config/connect.php');
    //connect to database
    $conn = mysqli_connect('localhost', 'sean', 'qazwsx321', 'approject');

    //check connection
    if(!$conn){
        echo 'Connection error' . mysqli_connect_error();
    }

    //write the query to show all users

    $sql = 'SELECT name, price, stock, pID FROM products ORDER BY name';

    //make query and get result
    $result = mysqli_query($conn, $sql);

    //fetch the resulting rows as an array
    $users = mysqli_fetch_all($result, MYSQLI_ASSOC);

    //free result form memory
    mysqli_free_result($result);
    
    //close connection to the database
    mysqli_close($conn);

    if(isset($_POST['delete'])){

    $id_to_delete = mysqli_real_escape_string($conn, $_POST['id_to_delete']);

    //make sql
    $sql = "DELETE FROM users WHERE pID = $id_to_delete";

    if(mysqli_query($conn, $sql))
    {
        header('Location: index.php');
        }else{
        echo 'query error: ' . mysqli_error($conn);
        }
    }



    //check GET request id parameter
    if(isset($_GET['pID']))
    {
        $id = mysqli_real_escape_string($conn, $_GET['pID']);

        //make sql
        $sql = "SELECT * FROM products WHERE pID = $pID";

        //get the query results
        $result = mysqli_query($conn, $sql);

        //fetch results in array format
        $user = mysqli_fetch_assoc($result);

        mysqli_free_result($result);
        mysqli_close($conn);
    }
*/
?>

<!DOCTYPE html>
<body>
    <h4 class = "center grey-text">Our Staff</h4>
    <div class="container">
        <div class="row">
            <?php foreach($products as $pro) { ?>
                <div class = "col s4 m3 l2">
                    <div class = "card z-depth-0">
                    <img class="imgc" src="coffeeImage.jpg" height = "100px" width = "100px">
                        <div class="card-content center">
                            <h4><?php echo htmlspecialchars($pro['name']); ?></h4>
                            <h6>_______________________________________________________________________</h6>
                            <p>Current price: <?php echo htmlspecialchars("$ ".$pro['price']); ?></p>
                            <p>Quantity availible: <?php echo htmlspecialchars($pro['stock']); ?> <br><br></p>
                            <p>Product ID: <?php echo htmlspecialchars($pro['pID']); ?></p>
                            <!--Delete form-->
                            <form action = "details.php" method="POST">
                            <input type="hidden" name= "id_to_delete" value="<?php echo $pro['pID'] ?>">
                            <input type="submit" name= "delete" value="Delete" class = "btn brand z-depth-0">
                            </form>
                        </div>
                    </div>
                </div>
            <?php }?>
        </div>
    </div>
</body>
<?php include('templates/footer.php'); ?>

</html><?php /**PATH C:\Users\sean9\Documents\ap\resources\views/details.blade.php ENDPATH**/ ?>