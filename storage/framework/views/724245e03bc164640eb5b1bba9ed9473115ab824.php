<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title>manager</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>

  <style>
      .b{
          background-image: url("background1.jpg");
          background-repeat: none;
          
      }
      #index-banner{
          float: center;
      }
      .card {
        border-radius: 25px;
        float:left;
        margin: 1.5%;
        width: 30%;
      }
      
      .t{
        background-color: rgba(255,255,255,0.3);
      }
      }.wrapper {
        max-width: 1140px;
        padding: 10px;
        margin: 10px auto;
        vertical-align: middle;
        background: blurred;
      }.product-index h1 {
        margin-bottom: 10px;
      }

      .product-index .product-item {
        margin: 10px 0;
        padding: 10px;
        vertical-align: middle;
        background: #f4f4f4;
      }

      .product-index .product-item img {
        
        vertical-align: sub;
        display: inline-block;
      }

      .product-index .product-item h4 {
        display: inline-block;
        font-weight: normal;
        margin-left: 20px;
      }

      .product-index .product-item h4 a {
        color: #777;
        text-decoration: none !important;
      }
  </style>

</head>

<body class = "b">
  <nav class="orange n" role="navigation">
    <div class="nav-wrapper container">
      <a id="logo-container" href="#" class="brand-logo">Products</a>
      <ul class="right hide-on-med-and-down">
        <?php if(auth()->check()): ?>
            <li>
                <a href="#" class="nav-link"><?php echo e(auth()->user()->firstname); ?></a>
            </li>
            <li>
                <a href="logout" class="nav-link">Log Out</a>
            </li>
        <?php else: ?> 
            <li class="nav-item">
                <a class="nav-link" href="signin">LOGIN</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="singup">REGISTER</a>
            </li>
        <?php endif; ?>
      </ul>

      <ul id="nav-mobile" class="sidenav">
        <li><a href="welcome">HOME</a></li>
        <li><a href="login">LOGIN</a></li>
        <li><a href="signup">REGISTER</a></li>
      </ul>
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
  </nav>

 
<section class = "container align-center">
    <h4 class = "center " style = "color:orange;">Products</h4>
    <br><br><br>
    <div class="wrapper t z-depth-4 product-index">          
      <table style = "color:black;" class="table striped">
        <thead >
          <tr>
            <th>ASL</th>
            <th>Item</th>
            <th>Item Number</th>
            <th>Name</th>
            <th>Category</th>
            <th>Cost</th>
            <th>Audio</th>
          </tr>
        </thead>
        <tbody>
          <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prod): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <tr>
            <td><img class= "img1 circle" height="50px" width="50px" src="<?php echo e(asset($prod->image)); ?>"></td>
            <td><img class= "img1 circle" height="50px" width="50px" src="<?php echo e(asset($prod->item_image)); ?>"></td>
            <td> <?php echo e($prod->id); ?></td>
            <td> <?php echo e($prod->name); ?></td>
            <td> <?php echo e($prod->category); ?></td>
            <td> <?php echo e($prod->cost); ?></td>
            <td><audio controls><source src="<?php echo e($prod->audio); ?>" type="audio/mpeg"> </audio></td>
            <td>
                <a href="<?php echo e(url('add-to-cart/'.$prod->id)); ?>"" class="btn orange darken-2">Add to Cart</a>
            </td>
          </tr>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
      </table>
    </div>
    
</section>

<div class="container">
  <p class="message center orange-text"><?php echo e(session('message')); ?></p>
</div>

  

  <!--
  <footer class="page-footer orange">
    <div class="footer-copyright">
      <div class="container">
      Made by <a class="brown-text text-lighten-3" href="http://materializecss.com">Materialize</a>
      </div>
    </div>
  </footer>
-->

  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>

  </body>
</html>
<?php /**PATH C:\Users\sean9\Documents\ap\resources\views/products.blade.php ENDPATH**/ ?>