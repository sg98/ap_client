<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title>manager</title>

  <!-- CSS  -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

  <style>
      .b{
          background-image: url("background1.jpg");
          background-repeat: none;
          text-decoration-color: orange;
      }
      #index-banner{
          float: center;
      }
      .card {
        border-radius: 25px;
        float:left;
        margin: 1.5%;
        width: 30%;
      }
      .card-image{
        border-top-right-radius: 25px;
        border-top-left-radius: 25px;
      }
  </style>

</head>

<body class = "b">
  <nav class="orange n" role="navigation">
    <div class="nav-wrapper container">
      <a id="logo-container" href="#" class="brand-logo">Manager</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="login">LOGIN</a></li>
        <li><a href="signup">REGISTER</a></li>
      </ul>

      <ul id="nav-mobile" class="sidenav">
        <li><a href="welcome">HOME</a></li>
        <li><a href="login">LOGIN</a></li>
        <li><a href="signup">REGISTER</a></li>
      </ul>
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
  </nav>

  <section class = "container align-center">
    <h4 class = "center" style = "color:orange;">All Items</h4>
    <br><br><br>
    <div class="wrapper Search">
    </div>
  </section>

  <div class=" container form-group">
    <form action="/searchfunc" method="get" >
        <br>
        <input style = "color:orange;" type='search' name="q" value="" placeholder="  Enter the Name of the item ">
        <button type ="submit" class="btn btn-default orange darken-2">Search</button>
        <br>
    </form>

    <?php if(isset($details)): ?>
      <div>
        <h4 style = "color:orange;">Results</h4>
        <table style = "color:orange;" class="table table-stripped">
          <thead >
            <tr>
              <th>Item Number</th>
              <th>Name</th>
              <th>Category</th>
              <th>Cost</th>
            </tr>
          </thead>
          <tbody>
            <?php $__currentLoopData = $details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
              <td> <?php echo e($item->id); ?></td>
              <td> <?php echo e($item->name); ?></td>
              <td> <?php echo e($item->category); ?></td>
              <td> <?php echo e($item->cost); ?></td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </tbody>
        </table>
      </div>

      <?php elseif(isset($message)): ?>
    <p style = "color:orange;"> <?php echo e($message); ?></p>
    <?php endif; ?>
      
  </div>
 
  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

  </body>
</html>
<?php /**PATH C:\Users\sean9\Documents\ap\resources\views/searchfunc.blade.php ENDPATH**/ ?>